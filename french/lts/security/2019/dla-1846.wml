#use wml::debian::translation-check translation="5b476e1721fdf82a8ea2adae59a642ed73c945f6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>David Fifield a découvert un moyen de construire des <q>bombes de
décompression</q> non récursives qui réalisent un taux de compression élevé
en superposant des fichiers dans un conteneur zip. Cependant, la taille de
sortie croît de manière quadratique par rapport à la taille d’entrée, atteignant
un taux de compression de plus de 28 millions (10 MB -&gt; 281 TB) à la limite
du format zip, ce qui peut causer un déni de service. Mark Adler a fourni un
correctif pour le programme unzip pour détecter et rejeter de tels fichiers zip.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 6.0-16+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets unzip.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1846.data"
# $Id: $
