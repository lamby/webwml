#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Samba, un serveur de
fichier SMB/CIFS, d'impression et de connexion pour Unix.

Le projet « Common vulnérabilités et Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14629">CVE-2018-14629</a>

<p>Florian Stuelpner a découvert que Samba est vulnérable à une récursion
infinie de requêtes provoquée par des boucles de CNAME, avec pour conséquence
un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16851">CVE-2018-16851</a>

<p>Garming Sam de l'équipe Samba et Catalyst ont découvert une vulnérabilité de
déréférencement de pointeur NULL dans le serveur LDAP de Samba AD DC permettant
à un utilisateur capable de lire plus de 256 Mo d'entrées LDAP de planter le
serveur LDAP de Samba AD DC.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 2:4.2.14+dfsg-0+deb8u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets samba.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1607.data"
# $Id: $
