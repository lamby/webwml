#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette DLA est une correction de DLA 680-1 qui mentionnait que
bash 4.2+dfsg-0.1+deb7u3 était corrigé. La version du paquet corrigé
était 4.2+dfsg-0.1+deb7u4.</p>

<p>Pour être complet, le texte de la DLA 680-1 est disponible ci-dessous
avec uniquement la correction des informations de version, sans autre
modification.</p>

<p>Un ancien vecteur d'attaque a été corrigé dans bash, un interpréteur de
langage de commande compatible à sh.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7543">CVE-2016-7543</a>

<p>Variables d'environnement SHELLOPTS+PS4 contrefaites pour l'occasion
en combinaison avec des binaires setuid non sûrs.</p>

<p>Le binaire setuid doit à la fois utiliser l'appel de fonction setuid()
et l'appel de fonction system() ou popen(). Avec cette combinaison, il est
possible d'accéder aux droits administrateurs.</p>

<p>En complément, bash doit être l'interpréteur de commande par défaut
(/bin/sh doit pointer vers bash) pour que le système soit vulnérable.</p>

<p>L'interpréteur de commande par défaut dans Debian est dash et il n'y a
pas de binaire setuid connu dans Debian avec la combinaison non sure
décrite ci-dessus.</p>

<p>Il pourrait néanmoins y avoir des logiciels locaux, avec la combinaison
non sure décrite ci-dessus, qui pourraient bénéficier de cette correction.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ce problème a été corrigé dans version
4.2+dfsg-0.1+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bash.</p>

<p>S'il y a des logiciels locaux qui ont cette combinaison non sure et qui
appliquent un setuid() à d'autres utilisateurs que le superutilisateur,
alors, cette mise à jour ne corrigera pas le problème. Il devra être traité
dans le binaire setuid non sûr.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-680-2.data"
# $Id: $
