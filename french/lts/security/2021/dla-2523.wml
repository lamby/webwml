#use wml::debian::translation-check translation="7e2e121907227c63046c343586a659966635cee3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été trouvées dans ImageMagick, une
suite de programmes de manipulation d’image. Un attaquant pourrait causer un
déni de service et une exécution de code arbitraire quand un fichier d’image
contrefait est traité.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14528">CVE-2017-14528</a>

<p>La fonction TIFFSetProfiles dans coders/tiff.c déduit de manière incorrecte
si les valeurs renvoyées par TIFFGetField de LibTIFF impliquent que leur validation
a été réalisée. Cela permet à des attaquants distants de provoquer un déni de
service (utilisation de mémoire après libération après d’un appel non valable
à TIFFSetField et plantage d'application) à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19667">CVE-2020-19667</a>

<p>Dépassement de pile et saut inconditionnel dans ReadXPMImage dans
coders/xpm.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25665">CVE-2020-25665</a>

<p>Le codeur d’image PALM dans coders/palm.c réalise un appel inapproprié
à AcquireQuantumMemory() dans la routine WritePALMImage() parce qu’un décalage
de 256 est nécessaire. Cela peut provoquer une lecture hors limites postérieure
dans la routine. Cela pourrait avoir un impact sur la fiabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25674">CVE-2020-25674</a>

<p>WriteOnePNGImage() de coders/png.c (le codeur PNG) intègre une boucle
avec une condition de sortie incorrecte, pouvant permettre une lecture hors
limites à l’aide d’un dépassement de tampon de tas. Cela se produit parce qu’il
est possible que la palette de couleurs ait moins de 256 valeurs mais que la
condition de boucle s’exécute 256 fois, essayant de passer des données non
valables de palette à l’enregistreur chronologique d’évènements.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27560">CVE-2020-27560</a>

<p>ImageMagick permet une division par zéro dans OptimizeLayerFrames dans
MagickCore/layer.c qui peut causer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27750">CVE-2020-27750</a>

<p>Un défaut a été découvert dans MagickCore/colorspace-private.h et
MagickCore/quantum.h. Un attaquant, soumettant un fichier contrefait qui est
traité, pourrait déclencher un comportement indéfini sous la forme de valeurs en
dehors de l’intervalle de type <em>unsigned char</em> et une division par zéro.
Cela impacterait probablement la disponibilité de l’application, mais pourrait,
éventuellement, causer d’autres problèmes relatifs à un comportement indéfini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27760">CVE-2020-27760</a>

<p>Dans GammaImage() de /MagickCore/enhance.c, selon la valeur <q>gamma</q>,
il est possible d’amener à une condition de division par zéro quand un fichier
d’entrée contrefait est traité par ImageMagick. Cela pourrait impacter la
disponibilité de l’application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27763">CVE-2020-27763</a>

<p>Un défaut a été découvert dans MagickCore/resize.c. Un attaquant, soumettant
un fichier contrefait qui est traité, pourrait déclencher un comportement
indéfini sous la forme d’une division par zéro. Cela impacterait probablement la
disponibilité de l’application, mais pourrait, éventuellement, causer d’autres
problèmes relatifs à un comportement indéfini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27765">CVE-2020-27765</a>

<p>Un défaut a été découvert dans MagickCore/segment.c. Un attaquant, soumettant
un fichier contrefait qui est traité, pourrait déclencher un comportement
indéfini sous la forme d’une division par zéro. Cela impacterait probablement la
disponibilité de l’application, mais pourrait, éventuellement, causer d’autres
problèmes relatifs à un comportement indéfini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27773">CVE-2020-27773</a>

<p>Un défaut a été découvert dans MagickCore/gem-private.h. Un attaquant,
soumettant un fichier contrefait qui est traité, pourrait déclencher un
comportement indéfini sous la forme de valeurs en dehors de l’intervalle de
type <em>unsigned char</em> ou une division par zéro. Cela impacterait
probablement la disponibilité de l’application, mais pourrait, éventuellement,
causer d’autres problèmes relatifs à un comportement indéfini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29599">CVE-2020-29599</a>

<p>ImageMagick gère incorrectement l’option <em>-authenticate</em>. Cela permet
de définir un mot de passe pour les fichiers PDF protégés par mot de passe.
Le mot de passe contrôlé par l’utilisateur n’était pas correctement protégé et
nettoyé. Il était donc possible d’injecter des commandes d’interpréteur
supplémentaires à l’aide de coders/pdf.c.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 8:6.9.7.4+dfsg-11+deb9u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de imagemagick, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/imagemagick">https://security-tracker.debian.org/tracker/imagemagick</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2523.data"
# $Id: $
