#use wml::debian::translation-check translation="73d8beb0919b558d19210d2e3c563ee30bfc61e9" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans les utilitaires de
recompression AdvanceCOMP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1056">CVE-2018-1056</a>

<p>Lecture hors limites de tampon de tas dans advzip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8379">CVE-2019-8379</a>

<p>Déréférencement de pointeur NULL dans be_uint32_read().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8383">CVE-2019-8383</a>

<p>Accès mémoire non valable dans adv_png_unfilter_8().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9210">CVE-2019-9210</a>

<p>Dépassement d'entier dans advpng avec une taille de PNG non valable.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.20-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets advancecomp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de advancecomp,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/advancecomp">\
https://security-tracker.debian.org/tracker/advancecomp</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2868.data"
# $Id: $

