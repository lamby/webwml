#use wml::debian::translation-check translation="534c79ca4cc91c44dffc95a54229d56d93ec8174" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans les utilitaires pour
cartes à puce OpenSC.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15945">CVE-2019-15945</a>

<p>Accès hors limites d'un type BitString ASN.1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15946">CVE-2019-15946</a>

<p>Accès hors limites d'un type Octet String ASN.1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19479">CVE-2019-19479</a>

<p>Opération de lecture incorrecte dans le pilote Setec.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26570">CVE-2020-26570</a>

<p>Dépassement de tampon de tas dans le pilote Oberthur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26571">CVE-2020-26571</a>

<p>Dépassement de tampon de pile dans le pilote GPK.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26572">CVE-2020-26572</a>

<p>Dépassement de tampon de pile dans le pilote TCOS.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 0.16.0-3+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets opensc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de opensc, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/opensc">\
https://security-tracker.debian.org/tracker/opensc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2832.data"
# $Id: $
