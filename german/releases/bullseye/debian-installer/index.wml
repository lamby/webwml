#use wml::debian::template title="Debian <q>Bullseye</q> -- Installer-Informationen" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="08a0f9fd40d72266eb96cec4b5e88f09ccc00215"

<h1>Installation von Debian <current_release_bullseye></h1>

<if-stable-release release="bookworm">
<p><strong>Debian 11 wurde durch
<a href="../../bookworm/">Debian 12 (<q>Bookworm</q>) ersetzt</a>.
Einige dieser Installations-Images könnten nicht mehr
verfügbar sein oder nicht mehr funktionieren.
Es wird daher empfohlen, stattdessen Bookworm zu installieren.
</strong></p>
</if-stable-release>

<if-stable-release release="bullseye">
<p>
<strong>Um Debian </strong> <current_release_bullseye>
(<em>Bullseye</em>) zu installieren, laden Sie eines der folgenden Images
herunter (unter i386 und amd64 können alle CD- und DVD-Images auch
auf USB-Sticks verwendet werden):
</p>

<div class="line">
<div class="item col50">
	<p><strong>Netzinstallations-CD-Images (netinst), typischerweise
170&ndash;470 MB</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>vollständige CD-Sätze</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>vollständige DVD-Sätze</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>andere Images (Netboot, spezielle USB-Sticks usw.)</strong></p>
<other-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Sollten Sie Hardware verwenden, deren Treiber <strong>das Laden nicht-freier
Firmware erfordert</strong>, können Sie einen der
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/bullseye/current/">\
Tarballs mit häufig verwendeten Firmware-Archiven</a> verwenden oder ein
<strong>inoffizielles</strong> Installations-Image herunterladen, das diese
<strong>nicht-freien</strong> Firmware-Dateien enthält. Eine
Anleitung zur Verwendung der Tarballs sowie allgemeine Informationen über
das Laden von Firmware während der Installation finden Sie auch in der
<a href="../amd64/ch06s04">Installationsanleitung</a>.
</p>
<div class="line">
<div class="item col50">
<p><strong>Netzinstallations-Images (typischerweise 240&ndash;290 MB), <strong>nicht-freie</strong>
CD-Images <strong>mit Firmare</strong></strong></p>
<small-non-free-cd-images />
</div>
</div>
</div>



<p>
<strong>Hinweise</strong>
</p>
<ul>
    <li>
	Für das Herunterladen kompletter CD- und DVD-Images wird die Verwendung
	von BitTorrent oder Jigdo empfohlen.
    </li><li>
	Für weniger gebräuchliche Architekturen ist nur eine begrenzte Anzahl von
	Images der CD- und DVD-Sätze als ISO-Datei oder über BitTorrent verfügbar.
	Die kompletten Sätze sind nur über Jigdo verfügbar.
    </li><li>
	Die Multi-arch <em>CD</em>-Images unterstützen die Installation auf
	i386/amd64; die Installation ist
	ähnlich der von Netzinstallations-Images für eine einzelne Architektur.
    </li><li>
	Das Multi-arch <em>DVD</em>-Image unterstützt die Installation auf
	i386/amd64; die Installation ist ähnlich der von vollständigen CD-/DVD-Images
	für eine einzelne Architektur; die DVD enthält auch die Quelltexte für alle
	auf der DVD enthaltenen Pakete.
    </li><li>
	Für die Installations-Images stehen Prüfsummen-Dateien (<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt> usw.) zur Verfügung; sie sind im gleichen Verzeichnis
	wie die Images zu finden.
    </li>
</ul>


<h1>Dokumentation</h1>

<p>
<strong>Falls Sie nur ein Dokument lesen möchten</strong>, bevor Sie installieren,
empfehlen wir Ihnen unser <a href="../i386/apa">Installations-HowTo</a>, ein
Schnelldurchgang durch den Installationsprozess. Weitere nützliche Dokumentation:
</p>

<ul>
<li><a href="../installmanual">Bullseye Installationsanleitung</a><br />
Detaillierte Anweisungen zur Installation</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installer FAQ</a>
und <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
Häufig gestellte Fragen und Antworten</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
Von der Gemeinschaft betreute Dokumentation</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Dies ist eine Liste von bekannten Problemen im Installer, der mit Debian
<current_release_bullseye> ausgeliefert wird. Falls Sie ein Problem bei der
Installation von Debian feststellen und dies Problem hier nicht aufgeführt ist,
senden Sie uns bitte einen
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">Installationsbericht</a>
(auf Englisch), in dem Sie das Problem beschreiben, oder schauen Sie
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">im Wiki</a>
nach anderen bekannten Problemen.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Errata für Release 11</h3>

<dl class="gloss">
     <dt>Firmware erforderlich für bestimmte Soundkarten</dt>

     <dd>Es gibt scheinbar eine Reihe von Soundkarten, die das Laden von
         Firmware erfordern, um Sound ausgeben zu können.
         Es ist dem Installer (Stand: Debian Bullseye) nicht möglich, diese
         Firmware früh im Installationsprozess zu laden, was bedeutet, dass
         Sprachausgabe (speech synthesis) während der Installation mit diesen
         Karten nicht möglich ist. Eine Möglichkeit, dies zu umgehen ist, eine
         andere Soundkarte zu stecken, die keine solche Firmware benötigt.
         Lesen Sie auch den
         <a href="https://bugs.debian.org/992699">übergeordneten Fehlerbericht</a>,
         um über unsere Bemühungen in dieser Sache auf dem Laufenden zu bleiben.
<!--
	 <br />
	 <b>Status:</b> Es ist unwahrscheinlich, dass noch mehr Anstrengungen
	 unternommen werden können, um weitere Pakete auf CD#1 zu bekommen.
-->
     </dd>
</dl>


<p>
Verbesserte Versionen des Installationssystems werden für das nächste
Debian-Release entwickelt und können auch verwendet werden, um Bullseye
zu installieren. Details finden Sie auf der
<a href="$(HOME)/devel/debian-installer/">Debian-Installer-Seite</a>.
</p>
</if-stable-release>
