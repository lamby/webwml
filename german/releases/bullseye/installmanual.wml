#use wml::debian::template title="Debian Bullseye &ndash; Installationsanleitung" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="83895cd0b0913e07ec4171a51420513771cf1c64"


<if-stable-release release="buster">
<p>Dies ist eine <strong>Betaversion</strong> der Installationsanleitung für
   Debian 11 (Codename Bullseye), die noch nicht veröffentlicht ist.
   Die hier vorgestellten Informationen können aufgrund von Änderungen am
   Installer veraltet und/oder inkorrekt sein. Sie sind vielleicht an der <a
   href="../buster/installmanual">Installationsanleitung für Debian 10
   (Codename Buster)</a> interessiert, welches die neueste veröffentlichte Version
   von Debian ist; oder an der <a href="https://d-i.debian.org/manual/">\
   Entwicklungsversion der Installationsanleitung</a>, die die aktuellste
   Version dieses Dokuments darstellt.
</p>
</if-stable-release>

<p>Installationsanleitungen (auch als Dateien zum Download) sind für
   alle unterstützten Architekturen verfügbar:</p>

<ul>
<:= &permute_as_list('', 'Installationsanleitung'); :>
</ul>

<p>Wenn Sie Ihren Browser richtig auf Ihre Sprache eingestellt haben, können
   Sie den obigen Link verwenden, um automatisch die richtige HTML-Version zu
   bekommen &ndash; siehe auch <a href="$(HOME)/intro/cn">\
   Inhalts-Aushandlung</a>. Andernfalls müssen Sie selber aus der folgenden
   Tabelle die richtige Architektur, Sprache und das Format aussuchen.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architektur</strong></th>
  <th align="left"><strong>Format</strong></th>
  <th align="left"><strong>Sprachen</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   "$_[0].$_[1].$_[2]" } ); :>
</table>
</div>
