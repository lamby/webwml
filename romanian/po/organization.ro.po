msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-09-20 12:52+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.8\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "scrisoare de delegare"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "scrisoare de numire în funcție"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegat"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegat"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"female\"/>delegat"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"female\"/>delegat"

#: ../../english/intro/organization.data:24
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"female\"/>delegat"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"female\"/>delegat"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "actual&#259;"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "membru"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "manager"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Administratorul Lansării Distribuției Stabile"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "asistent de configurare"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
#, fuzzy
msgid "chair"
msgstr "președinte"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "asistent"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "secretar"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Ofiteri"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distribuție"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
msgid "Publicity team"
msgstr "Echipa de publicitate"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "Suport și Infrastructură"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Conducător"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Comitet Tehnic"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Secretar"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Proiecte de dezvoltare"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "Arhive FTP"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "FTP Masters"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "Asistenți FTP"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "Asistenți de configurare FTP"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Backport-uri"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Echipa pentru backport-uri"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Administrarea lansărilor"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Echipa de lansare"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Controlul calit&#259;&#355;ii"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Echipa de Instalare Sisteme"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "Note de lansare"

#: ../../english/intro/organization.data:152
#, fuzzy
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "Imagini CD"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Producere"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "Testare"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:167
msgid "Autobuilding infrastructure"
msgstr "Infrastructură auto-construibilă"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr "Echipa Wanna-build"

#: ../../english/intro/organization.data:176
msgid "Buildd administration"
msgstr "Administrarea de build-uri"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "documentație"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr " Potențiale pachete noi sau pachete ce necesită lucru"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr "Contact de presă"

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "Pagini Web"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr "Mobilizare"

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr "Proiectul Debian pentru Femei"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "Evenimente"

#: ../../english/intro/organization.data:264
#, fuzzy
msgid "DebConf Committee"
msgstr "Comitet Tehnic"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "Program de parteneriat"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr "Coordonarea donațiilor hardware"

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "Sistem de urmărire pentru bug-uri"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administrare Liste de discuții și Liste de arhive"

#: ../../english/intro/organization.data:329
msgid "New Members Front Desk"
msgstr "Recepție pentru membri noi"

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "Administratori de conturi Debian"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Menținătorii Keyring-ului (PGP și GPG)"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "Echipa de Securitate"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "Regulament"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "Administrarea Sistemului"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Dacă ai probleme în a folosi mașinile puse la dispoziție de Debian "
"(incluzând schimbarea parolei sau dacă ai nevoie de software), aceasta este "
"adresa pe care o poți folosi. "

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Dacă ai probleme de hardware cu o mașină Debian, te rugăm sa consulți pagina "
"<a href=\"https://db.debian.org/machines.cgi\">Debian Machines</a>, ce "
"conține informații despre administrarea fiecărei mașini în parte."

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr "Administrator LDAP"

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "Oglinzi"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "Administrator DNS"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "Sistem de urmărire a pachetelor"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Cereri pentru folosirea <a name=\"trademark\" href=\"m4_HOME/trademark"
"\">Mărcii înregistrate</a> "

#: ../../english/intro/organization.data:392
#, fuzzy
msgid "Salsa administrators"
msgstr "Administratori Alioth"

#~ msgid "Anti-harassment"
#~ msgstr "Anti-hărțuire"

#~ msgid "Debian Pure Blends"
#~ msgstr "Blenduri Pure Debian"

#~ msgid "Individual Packages"
#~ msgstr "Pachete individuale"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian pentru copii cu vârste între 1 și 99 de ani"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian pentru practică medicală și cercetare"

#~ msgid "Debian for education"
#~ msgstr "Debian pentru educație"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian pentru birouri de avocatură"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian pentru persoane cu dizabilități"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian pentru știință și cercetare"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian pentru educație"

#~ msgid "Live System Team"
#~ msgstr "Echipa pentru sisteme live"

#~ msgid "Auditor"
#~ msgstr "Auditor"

#~ msgid "Publicity"
#~ msgstr "Publicitate"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Menținătorii Keyring-ului menținătorilor Debian"

#~ msgid "DebConf chairs"
#~ msgstr "Delegați DebConf"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "echipa de securitate"

#, fuzzy
#~ msgid "Release Assistants"
#~ msgstr "note de lansare"

#, fuzzy
#~ msgid "Release Assistants for ``stable''"
#~ msgstr "administratorul lans&#259;rii pentru &#8222;stable&#8221;"

#, fuzzy
#~ msgid "Release Wizard"
#~ msgstr "administratorul lans&#259;rii"

#~ msgid "APT Team"
#~ msgstr "Echipa APT"

#~ msgid "Internal Projects"
#~ msgstr "proiecte interne"

#~ msgid "Mailing List Archives"
#~ msgstr "arhivele listelor de e-mail"

#~ msgid "Installation System for ``stable''"
#~ msgstr "echipa systemului de instalare pentru &#8222;stable&#8221;"

#, fuzzy
#~ msgid "Marketing Team"
#~ msgstr "echipa de securitate"

#~ msgid "Vendors"
#~ msgstr "v&#226;nz&#259;tori"

#, fuzzy
#~ msgid "Release Team for ``stable''"
#~ msgstr "administratorul lans&#259;rii pentru &#8222;stable&#8221;"

#, fuzzy
#~ msgid "Custom Debian Distributions"
#~ msgstr "distribu&#355;ie"

#~ msgid "Alioth administrators"
#~ msgstr "Administratori Alioth"

#~ msgid "User support"
#~ msgstr "Suport pentru utilizatori"

#~ msgid "Embedded systems"
#~ msgstr "Sisteme integrate"

#~ msgid "Firewalls"
#~ msgstr "Firewall-uri"

#~ msgid "Laptops"
#~ msgstr "Laptop-uri"

#~ msgid "Special Configurations"
#~ msgstr "Configurații speciale"

#~ msgid "Ports"
#~ msgstr "Porturi"

#~ msgid "CD Vendors Page"
#~ msgstr "Pentru Comercianți de CD-uri"

#~ msgid "Consultants Page"
#~ msgstr "Pentru Consultanți"
