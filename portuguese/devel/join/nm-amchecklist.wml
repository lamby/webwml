#use wml::debian::template title="Checklist para gestores(as) de candidaturas (AM)"
#use wml::debian::translation-check translation="5011f532637dc7820b79b151eecfda4ab65aa22f"

<p>
<b>Nota:</b> a página wiki
<a href="https://wiki.debian.org/FrontDesk/AMTutorial">tutorial do(a) gestor(a)
de candidatura (AM)</a> está mais atualizada que esta página.
</p>

<p>Este checklist cobre apenas as áreas mais importantes das verificações do(a)
novo(a) membro(a) (NM).
Dependendo do histórico e dos planos do(a)
<a href="./newmaint#Applicant">candidato(a)</a> no projeto, um(a)
<a href="./newmaint#AppMan">gestor(a) de candidatura</a> pode optar por ignorar
algumas coisas listadas aqui ou adicionar outras.</p>

<p>Veja também o <a href="nm-amhowto">MiniHOWTO para gestor(a) de
candidatura</a>.</p>

<h3><a name="identification">Verificação de identificação</a></h3>
<p>O(A) <a href="./newmaint#Applicant">candidato(a)</a> deve ter uma chave
pública OpenPGP assinada por pelo menos um(a)
<a href="./newmaint#Member">membro(a) do Debian</a>. Também é solicitada,
se possível, pelo menos uma outra assinatura de uma chave OpenPGP bem conectada.
Sempre use <tt>gpg --check-sigs</tt>, e não <tt>gpg --list-sigs</tt> para
verificar a identidade do(a) candidato(a).</p>

<p>A chave OpenPGP que irá para o chaveiro Debian precisa ser uma chave versão
4. Para verificar isso, obtenha o fingerprint da chave e verifique se tem o
tamanho de 32 ou 40 dígitos hexadecimais. As chaves versão 3 têm apenas 32
dígitos, a versão 4 tem 40 dígitos. Essa chave não precisa ser a mesma que é
usada para verificar a identidade do(a) candidato(a).</p>

<p>O(A) candidato(a) <em>deve</em> ter uma chave de encriptação. Verifique isso
executando <tt>gpg --list-keys <var>&lt;KeyID&gt;</var></tt>.
Se a saída não tiver uma linha
<tt><var>&lt;Number&gt;</var>E/<var>&lt;KeyID&gt;</var></tt> ou
<tt><var>&lt;Number&gt;</var>g/<var>&lt;KeyIDe&gt;</var></tt>, o(a)
candidato(a) precisa adicionar uma subchave de encriptação.</p>

<p>Se o(a) <a href="./newmaint#Applicant">candidato(a)</a> não puder fornecer
uma chave assinada, uma identificação com foto emitida pelo governo pode ser
usada para identificação. Por favor entre em contato com a
<a href="./newmaint#FrontDesk">secretaria</a> nesse caso.</p>

<p>Opções de verificação adicionais podem ser usadas se houver alguma dúvida
sobre a identidade do(a) candidato(a): </p>

<ul>
 <li>Se o(a) candidato(a) for estudante, alguém da sua universidade poderá
  confirmar sua identidade. Essa pessoa também deve estar listada nas páginas
  web dos funcionários da universidade. </li>

 <li>Se o(a) candidato(a) trabalha em uma empresa grande, seu empregador deve
  ser capaz de confirmar sua identidade.</li>

 <li>Existem sites web que podem fazer pesquisas reversa para números de
  telefone, embora isso normalmente não funcione para telefones celulares.
  O número fornecido pelo(a) candidato(a) deve estar no seu próprio nome ou a
  pessoa que atender o telefone deve ser capaz de confirmar a identidade
  do(a) candidato(a).</li>
</ul>

<h3><a name="pandp">Filosofia e procedimentos</a></h3>
<p>Não há regras fixas para esta parte, mas algumas áreas sempre devem ser
cobertas (e é recomendável discutir as outras): </p>
<ul>
 <li>O(A) candidato(a) deve concordar em apoiar a <a
  href="$(DOC)/debian-policy/">política Debian</a> e a <a
  href="$(DEVEL)/dmup">política de uso de máquinas Debian (DMUP)</a>.</li>

 <li>O(A) candidato(a) precisa concordar com o <a href="$(HOME)/social_contract">\
  contrato social</a> e deve ser capaz de explicar como o Debian se relaciona
  com a comunidade de Software Livre.</li>

 <li>O(A) candidato(a) deve ter um bom entendimento da <a
  href="$(HOME)/social_contract#guidelines">definição Debian de software
  livre</a>. Ele(a) precisa ser capaz de decidir se uma licença é livre ou não
  e deve ter uma opinião forte sobre Software Livre.</li>

 <li>O(A) candidato(a) deve entender como o sistema de acompanhamento de bugs
  Debian funciona, quais informações o Debian mantém lá (pseudopacotes,
  wnpp, ...), e como ele(a) pode manipular o sistema.</li>

 <li>O(A) candidato(a) deve conhecer os processos de QA em pacotes do Debian
 (deixar órfão, remover, fazer NMU e fazer uploads de QA).</li>

 <li>O(A) candidato(a) deve entender o processo de lançamento do Debian.</li>

 <li>O(A) candidato(a) deve conhecer os esforços l10n e i18n do Debian e o que
  ele(a) pode fazer para ajudá-los. </li>
</ul>

<h3><a name="tands">Tarefas e habilidades</a></h3>
<p>O que precisa ser coberto pelas verificações de tarefas e habilidades
depende da área em que o(a) candidato(a) deseja trabalhar:</p>

<ul>
 <li>O(a) candidato(a) que pretende trabalhar como empacotador(a) <em>deve </em>
  ter pelo menos um pacote no repositório. O pacote deve ter usuários(as)
  suficientes para fornecer uma base para a documentação das habilidades de
  empacotamento do(a) candidato(a) e sua maneira de lidar com usuários(as),
  remetentes de bugs e bugs.
  <br />
  Outras questões também devem cobrir alguns aspectos básicos do empacotamento
  Debian (arquivos de configuração, menus, scripts init, subpolíticas,
  portabilidade, dependências complexas).</li>

 <li>O(A) candidato(a) que planeja escrever documentação já deve ter fornecido
  exemplos de seus trabalhos. Ele(a) também deve ter uma visão clara sobre que
  tipo de documentos deseja trabalhar no futuro. </li>
</ul>

<h3><a name="finalreport">Relatório final da candidatura para o(a) gestor(a) de contas Debian</a></h3>
<p>Depois que todas as verificações forem concluídas e o(a) gestor(a) de
candidatura (AM) estiver satisfeito(a) com o desempenho do(a) candidato(a), um
relatório deve ser enviado ao(a) gestor(a) de contas Debian e à secretaria de
novos(as) membros(as). Este relatório deverá documentar o que foi feito para
satisfazer as diferentes partes das verificações de novos(as) membros(as) e
também deve conter todas as informações coletadas sobre o(a) candidato(a).</p>

<p>O e-mail deve ser enviado para &lt;da-manager@debian.org&gt; e
 &lt;nm@debian.org&gt; contendo as seguintes coisas:</p>

<ul>
 <li>Uma visão geral curta sobre a candidatura, contendo algumas informações
  básicas sobre o(a) candidato(a).</li>

 <li>O nome da conta que o(a) candidato(a) solicitou. Ela deve ter o tamanho
  de pelo menos 3 caracteres.</li>

 <li>O endereço de e-mail para o qual todas as mensagens enviadas diretamente
  para <var>&lt;conta&gt;</var>@debian.org serão redirecionadas.</li>

 <li>O fingerprint da chave pública OpenPGP do(a) candidato(a) que deve ser
  incorporada ao chaveiro Debian.</li>

 <li>Uma mbox compactada com gzip com logs de todas as discussões entre o(a)
  gestor(a) de candidatura e o(a) candidato(a) sobre a candidatura.</li>
</ul>

<p>Isso completa as responsabilidades do(a) gestor(a) de candidatura no processo
de candidatura. A secretaria de novos(as) membros(as) e o(a) gestor(a) de contas
verificarão e julgarão o relatório da candidatura.</p>

<hr />
<a href="newmaint">Volte para o canto dos(as) novos(as) membros(as)</a>
