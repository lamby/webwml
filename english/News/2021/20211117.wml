# Status: published
# $Id$
# $Rev$

<define-tag pagetitle>Statement on Daniel Pocock</define-tag>
<define-tag release_date>2021-11-17</define-tag>
#use wml::debian::news

<p>Debian is aware of a number of public posts made about Debian and its 
community members on a series of websites by a Mr Daniel Pocock, who purports 
to be a Debian Developer.</p>


<p>Mr Pocock is not associated with Debian. He is neither a Debian Developer, 
nor a member of the Debian community.  He was formerly a Debian Developer, but was
expelled from the project some years ago for engaging in behaviour which was
destructive to Debian's reputation and to the community itself.  He has not
been a member of the Debian Project since 2018. He is also banned from
participating in the Debian community in any form, including through technical
contributions, participating in online spaces, or attending conferences and/or
events. He has no right or standing to represent Debian in any capacity, or to
represent himself as a Debian Developer or member of the Debian community.</p>

<p>In the time since he was expelled from the project, Mr Pocock has engaged in 
an ongoing and extensive campaign of retaliatory harassment by making a number 
of inflammatory and defamatory posts online, in particular on a website which 
purports to be a Debian website. The contents of these posts involve not only
Debian, but also a number of its Developers and volunteers.  He has also
continued to misrepresent himself as being a member of the Debian Community in
much of his communication and public presentations.  

Please see <a href="https://bits.debian.org/2020/03/official-communication-channels.html">this article</a>
for a list of the official Debian communication channels.

Legal action is being
considered for, amongst other things, defamation, malicious falsehood and
harassment.</p>

<p>Debian stands together as a community, and against harassment. We have a code
of conduct that guides our response to harmful behaviour in our community, and
we will continue to act to protect our community and volunteers.
Please do not hesitate to contact the Debian Community team if you have
concerns or need support.  In the meantime, all of Debian's and its volunteers'
rights are reserved.</p>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely free
operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.</p>



