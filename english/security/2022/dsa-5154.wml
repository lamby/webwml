<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the WebKitGTK
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26700">CVE-2022-26700</a>

    <p>ryuzaki discovered that processing maliciously crafted web content
    may lead to code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26709">CVE-2022-26709</a>

    <p>Chijin Zhou discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26716">CVE-2022-26716</a>

    <p>SorryMybad discovered that Processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26717">CVE-2022-26717</a>

    <p>Jeonghoon Shin discovered that Processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26719">CVE-2022-26719</a>

    <p>Dongzhuo Zhao discovered that Processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30293">CVE-2022-30293</a>

    <p>Chijin Zhou discovered that processing maliciously crafted web
    content may lead to arbitrary code execution or to a denial of
    service (application crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30294">CVE-2022-30294</a>

    <p>Chijin Zhou discovered that processing maliciously crafted web
    content may lead to arbitrary code execution or to a denial of
    service (application crash).</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 2.36.3-1~deb10u1.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.36.3-1~deb11u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5154.data"
# $Id: $
