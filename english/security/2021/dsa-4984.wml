<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that sandbox restrictions in Flatpak, an application
deployment framework for desktop apps, could be bypassed for a Flatpak
app with direct access to AF_UNIX sockets, by manipulating the VFS using
mount-related syscalls that are not blocked by Flatpak's denylist
seccomp filter.</p>

<p>Details can be found in the upstream advisory at
<a href="https://github.com/flatpak/flatpak/security/advisories/GHSA-67h7-w3jq-vh4q">\
https://github.com/flatpak/flatpak/security/advisories/GHSA-67h7-w3jq-vh4q</a></p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 1.10.5-0+deb11u1.</p>

<p>We recommend that you upgrade your flatpak packages.</p>

<p>For the detailed security status of flatpak please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/flatpak">\
https://security-tracker.debian.org/tracker/flatpak</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4984.data"
# $Id: $
