<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Jann Horn discovered that the ptrace subsystem in the Linux kernel
mishandles the management of the credentials of a process that wants to
create a ptrace relationship, allowing a local user to obtain root
privileges under certain scenarios.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 4.9.168-1+deb9u4.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 4.19.37-5+deb10u1. This update includes as well a patch for a
regression introduced by the original fix for <a href="https://security-tracker.debian.org/tracker/CVE-2019-11478">CVE-2019-11478</a> (#930904).</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4484.data"
# $Id: $
