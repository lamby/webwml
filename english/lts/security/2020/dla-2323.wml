<define-tag description>LTS new package</define-tag>
<define-tag moreinfo>
<p>Linux 4.19 has been packaged for Debian 9 as linux-4.19.  This
provides a supported upgrade path for systems that currently use
kernel packages from the "stretch-backports" suite.</p>

<p>There is no need to upgrade systems using Linux 4.9, as that kernel
version will also continue to be supported in the LTS period.</p>

<p>This backport does not include the following binary packages:</p>

<blockquote><div>hyperv-daemons libbpf-dev libbpf4.19 libcpupower-dev libcpupower1
liblockdep-dev liblockdep4.19 linux-compiler-gcc-6-arm
linux-compiler-gcc-6-x86 linux-cpupower linux-libc-dev lockdep
usbip</div></blockquote>

<p>Older versions of most of those are built from the linux source
package in Debian 9.</p>

<p>The kernel images and modules will not be signed for use on systems
with Secure Boot enabled, as there is no support for this in Debian 9.</p>

<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a denial of service or information leak.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18814">CVE-2019-18814</a>

    <p>Navid Emamdoost reported a potential use-after-free in the
    AppArmor security module, in the case that audit rule
    initialisation fails.  The security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18885">CVE-2019-18885</a>

    <p>The <q>bobfuzzer</q> team discovered that crafted Btrfs volumes could
    trigger a crash (oops).  An attacker able to mount such a volume
    could use this to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20810">CVE-2019-20810</a>

    <p>A potential memory leak was discovered in the go7007 media driver.
    The security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10766">CVE-2020-10766</a>

    <p>Anthony Steinhauser reported a flaw in the mitigation for
    Speculative Store Bypass (<a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">CVE-2018-3639</a>) on x86 CPUs.  A local
    user could use this to temporarily disable SSB mitigation in other
    users' tasks.  If those other tasks run sandboxed code, this would
    allow that code to read sensitive information in the same process
    but outside the sandbox.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10767">CVE-2020-10767</a>

    <p>Anthony Steinhauser reported a flaw in the mitigation for Spectre
    variant 2 (<a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">CVE-2017-5715</a>) on x86 CPUs.  Depending on which other
    mitigations the CPU supports, the kernel might not use IBPB to
    mitigate Spectre variant 2 in user-space.  A local user could use
    this to read sensitive information from other users' processes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10768">CVE-2020-10768</a>

    <p>Anthony Steinhauser reported a flaw in the mitigation for Spectre
    variant 2 (<a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">CVE-2017-5715</a>) on x86 CPUs.  After a task force    disabled indirect branch speculation through prctl(), it could
    still re-enable it later, so it was not possible to override a
    program that explicitly enabled it.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12655">CVE-2020-12655</a>

    <p>Zheng Bin reported that crafted XFS volumes could trigger a system
    hang.  An attacker able to mount such a volume could use this to
    cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12771">CVE-2020-12771</a>

    <p>Zhiqiang Liu reported a bug in the bcache block driver that could
    lead to a system hang.  The security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13974">CVE-2020-13974</a>

    <p>Kyungtae Kim reported a potential integer overflow in the vt
    (virtual terminal) driver.  The security impact of this is
    unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15393">CVE-2020-15393</a>

    <p>Kyungtae Kim reported a memory leak in the usbtest driver.  The
    security impact of this is unclear.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
4.19.132-1~deb9u1.  This update additionally fixes Debian bugs
<a href="https://bugs.debian.org/958300">#958300</a>, <a href="https://bugs.debian.org/960493">#960493</a>, <a href="https://bugs.debian.org/962254">#962254</a>, <a href="https://bugs.debian.org/963493">#963493</a>, <a href="https://bugs.debian.org/964153">#964153</a>, <a href="https://bugs.debian.org/964480">#964480</a>, and <a href="https://bugs.debian.org/965365">#965365</a>; and
includes many more bug fixes from stable updates 4.19.119-4.19.132
inclusive.</p>

<p>We recommend that you upgrade your linux-4.19 packages.</p>

<p>For the detailed security status of linux-4.19 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux-4.19">https://security-tracker.debian.org/tracker/linux-4.19</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2323.data"
# $Id: $
