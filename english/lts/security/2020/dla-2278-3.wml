<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update of squid3 released as DLA-2278-2 introduced a regression
due to the updated fix for <a href="https://security-tracker.debian.org/tracker/CVE-2019-12529">CVE-2019-12529</a>. The new Kerberos
authentication code prevented base64 token negotiation. Updated squid3
packages are now
available to correct this issue.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.5.23-5+deb9u4.</p>

<p>We recommend that you upgrade your squid3 packages.</p>

<p>For the detailed security status of squid3 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/squid3">https://security-tracker.debian.org/tracker/squid3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2278-3.data"
# $Id: $
