<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Serious vulnerabilities were found in the libvorbis library, commonly
used to encode and decode audio in OGG containers.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14633">CVE-2017-14633</a>

     <p>In Xiph.Org libvorbis 1.3.5, an out-of-bounds array read
     vulnerability exists in the function mapping0_forward() in
     mapping0.c, which may lead to DoS when operating on a crafted
     audio file with vorbis_analysis().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14632">CVE-2017-14632</a>

     <p>Xiph.Org libvorbis 1.3.5 allows Remote Code Execution upon
     freeing uninitialized memory in the function
     vorbis_analysis_headerout() in info.c when vi->channels<=0, a
     similar issue to Mozilla bug 550184.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11333">CVE-2017-11333</a>

     <p>The vorbis_analysis_wrote function in lib/block.c in Xiph.Org
     libvorbis 1.3.5 allows remote attackers to cause a denial of
     service (OOM) via a crafted wav file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5146">CVE-2018-5146</a>

     <p>out-of-bounds memory write in the codeboook parsing code of the
     Libvorbis multimedia library could result in the execution of
     arbitrary code.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.2-1.3+deb7u1.</p>

<p>We recommend that you upgrade your libvorbis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1368.data"
# $Id: $
