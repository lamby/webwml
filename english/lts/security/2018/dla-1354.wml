<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were found in OpenCV, the <q>Open Computer Vision
Library</q>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5268">CVE-2018-5268</a>

    <p>In OpenCV 3.3.1, a heap-based buffer overflow happens in
    cv::Jpeg2KDecoder::readComponent8u in
    modules/imgcodecs/src/grfmt_jpeg2000.cpp when parsing a crafted
    image file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5269">CVE-2018-5269</a>

    <p>In OpenCV 3.3.1, an assertion failure happens in
    cv::RBaseStream::setPos in modules/imgcodecs/src/bitstrm.cpp
    because of an incorrect integer cast.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.3.1-11+deb7u4.</p>

<p>We recommend that you upgrade your opencv packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1354.data"
# $Id: $
