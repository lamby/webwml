<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in zziplib, a library providing read access on
ZIP-archive.
Because of mishandling a return value, an attacker might cause a denial of
service due to an infinite loop.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
0.13.62-3.2~deb9u2.</p>

<p>We recommend that you upgrade your zziplib packages.</p>

<p>For the detailed security status of zziplib please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/zziplib">https://security-tracker.debian.org/tracker/zziplib</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2859.data"
# $Id: $
