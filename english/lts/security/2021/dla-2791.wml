<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential remote privilege escalation
vulnerability in the Mailman mailing-list manager. Some CSRF token values were
derived from the admin password, and that could have been used to conductg a
brute-force attack against that password.</p>

<ul>

<li>
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-42096">CVE-2021-42096</a>
    &amp;
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-42097">CVE-2021-42097</a>

    <p>GNU Mailman before 2.1.35 may allow remote Privilege Escalation. A
    certain csrf_token value is derived from the admin password, and may be
    useful in conducting a brute-force attack against that password.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:2.1.23-1+deb9u7.</p>

<p>We recommend that you upgrade your mailman packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2791.data"
# $Id: $
