<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Mechanize is an open-source Ruby library that makes automated web
interaction easy. In Mechanize, from v2.0.0 until v2.7.7, there
is a command injection vulnerability.</p>

<p>Affected versions of Mechanize allow for OS commands to be
injected using several classes' methods which implicitly use
Ruby's Kernel#open method.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.7.5-1+deb9u1.</p>

<p>We recommend that you upgrade your ruby-mechanize packages.</p>

<p>For the detailed security status of ruby-mechanize please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-mechanize">https://security-tracker.debian.org/tracker/ruby-mechanize</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2561.data"
# $Id: $
