<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Private files that have been uploaded by an anonymous user but not permanently
attached to content on the site should only be visible to the anonymous user
that uploaded them, rather than all anonymous users. Drupal core did not
previously provide this protection, allowing an access bypass vulnerability to
occur. This issue is mitigated by the fact that in order to be affected, the
site must allow anonymous users to upload files into a private file system.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.14-2+deb7u16.</p>

<p>We recommend that you upgrade your drupal7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1004.data"
# $Id: $
