<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service, or information
leak.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12207">CVE-2018-12207</a>

    <p>It was discovered that on Intel CPUs supporting hardware
    virtualisation with Extended Page Tables (EPT), a guest VM may
    manipulate the memory management hardware to cause a Machine Check
    Error (MCE) and denial of service (hang or crash).</p>

    <p>The guest triggers this error by changing page tables without a
    TLB flush, so that both 4 KB and 2 MB entries for the same virtual
    address are loaded into the instruction TLB (iTLB).  This update
    implements a mitigation in KVM that prevents guest VMs from
    loading 2 MB entries into the iTLB.  This will reduce performance
    of guest VMs.</p>

    <p>Further information on the mitigation can be found at
    <url "https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/multihit.html">
    or in the linux-doc-4.9 package.</p>

    <p>Intel's explanation of the issue can be found at
    <a href="https://software.intel.com/security-software-guidance/insights/deep-dive-machine-check-error-avoidance-page-size-change-0">software.intel.com/…/deep-dive-machine-check-error-avoidance-page-size-change-0</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0154">CVE-2019-0154</a>

    <p>Intel discovered that on their 8th and 9th generation GPUs,
    reading certain registers while the GPU is in a low-power state
    can cause a system hang.  A local user permitted to use the GPU
    can use this for denial of service.</p>

    <p>This update mitigates the issue through changes to the i915
    driver.</p>

    <p>The affected chips (gen8 and gen9) are listed at
    <a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen8">en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0155">CVE-2019-0155</a>

    <p>Intel discovered that their 9th generation and newer GPUs are
    missing a security check in the Blitter Command Streamer (BCS).  A
    local user permitted to use the GPU could use this to access any
    memory that the GPU has access to, which could result in a denial
    of service (memory corruption or crash), a leak of sensitive
    information, or privilege escalation.</p>

    <p>This update mitigates the issue by adding the security check to
    the i915 driver.</p>

    <p>The affected chips (gen9 onward) are listed at
    <a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen9">en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11135">CVE-2019-11135</a>

    <p>It was discovered that on Intel CPUs supporting transactional
    memory (TSX), a transaction that is going to be aborted may
    continue to execute speculatively, reading sensitive data from
    internal buffers and leaking it through dependent operations.
    Intel calls this <q>TSX Asynchronous Abort</q> (TAA).</p>

    <p>For CPUs affected by the previously published Microarchitectural
    Data Sampling (MDS) issues (<a href="https://security-tracker.debian.org/tracker/CVE-2018-12126">CVE-2018-12126</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12127">CVE-2018-12127</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-12130">CVE-2018-12130</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-11091">CVE-2019-11091</a>), the existing mitigation also
    mitigates this issue.</p>

    <p>For processors that are vulnerable to TAA but not MDS, this update
    disables TSX by default.  This mitigation requires updated CPU
    microcode.  An updated intel-microcode package (only available in
    Debian non-free) will be provided via a future DLA.  The updated
    CPU microcode may also be available as part of a system firmware
    ("BIOS") update.</p>

    <p>Further information on the mitigation can be found at
    <a href="https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/tsx_async_abort.html">www.kernel.org/…/tsx_async_abort.html</a>
    or in the linux-doc-4.9 package.</p>

    <p>Intel's explanation of the issue can be found at
    <a href="https://software.intel.com/security-software-guidance/insights/deep-dive-intel-transactional-synchronization-extensions-intel-tsx-asynchronous-abort">software.intel.com/…/deep-dive-intel-transactional-synchronization-extensions-intel-tsx-asynchronous-abort</a>.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.9.189-3+deb9u2~deb8u1.</p>

<p>We recommend that you upgrade your linux-4.9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1990.data"
# $Id: $
