<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tavis Ormandy discovered that the BN_mod_sqrt() function of OpenSSL
could be tricked into an infinite loop. This could result in denial of
service via malformed certificates.</p>

<p>In addition, this update fixes an overflow bug in the x64_64 Montgomery
squaring procedure.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.1.0l-1~deb9u5.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">https://security-tracker.debian.org/tracker/openssl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2952.data"
# $Id: $
