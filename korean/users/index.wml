#use wml::debian::template title="누가 데비안을 쓰나요?" BARETITLE=true
#use wml::debian::toc
#use wml::debian::users_list
#use wml::debian::translation-check translation="d4327894f56ad37ddeab0cfc32e3b096993e9766" maintainer="Sebul"

<p>여기 포함된 것은 데비안을 배치한 일부 기관의 설명, 그리고 어떻게 데비안을 사용하는지와 왜 선택했는지 간단한 설명입니다.
항목은 알파벳 순입니다.
여러분이 이 목록에 포함되고 싶으면 <a href="#submissions">지침</a>을 따르세요.
</p>

<toc-display />

<toc-add-entry name="edu">교육 기관</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'edu', '.*') :>

<toc-add-entry name="com">상업용</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'com', '.*') :>

<toc-add-entry name="org">비영리 단체</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'org', '.*') :>

<toc-add-entry name="gov">정부 기관</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'gov', '.*') :>

<hr />

<h2><a name="submissions" id="submissions">제출</a></h2>

<p>이 목록에 추가되려면, 아래 정보를
  <a
  href="mailto:debian-www@lists.debian.org?subject=Who's%20using%20Debian%3F">debian-www@lists.debian.org</a>에 
  메일을 보내세요(제목 줄을 바꾸지 마세요).
  영어로 보내야 합니다. 영어를 쓰지 않으면,
  <a href="https://lists.debian.org/i18n.html">적절한 번역 리스트</a>에 쓰세요.
제출 메일은 공개 메일링 리스트에 게시됨을 주의하세요.
</p>
<p>여러분이 대표하는 기관에 대한 정보만 내세요.
여기에서 내기 전에 기관 내부에서 권한을 요청해야 할 수도 있습니다.
데비안에 대한 <strong>유료</strong> 지원을 통해 수입의 적어도 일부를 차지하면 <a href="$(HOME)/consultants/">Consultants</a> 페이지를 들르세오.
</p>

<p>
개인정보를 제출하지 마세요.
우리는 여러분을 지원을 가치있게 생각하지만, 여러분의 많은 개인정보를 유지할 충분한 사람이 없습니다.
</p>

<p>
  <strong>공개 지식이 되는 데 편한 정보만 넣으세요.</strong>
</p>

<ol>
  <li>
    <p>
기관 이름(형식은 <em>분야</em>, <em>기관</em>, <em>도시</em> (선택), <em>나라</em>)
예: AI Lab, Massachusetts Institute of Technology, USA
    </p>
    <p>
모든 항목이 이 형식으로 되어 있지는 않지만, 변환하려고 합니다.</p>
  </li>
  <li>기관 유형 (교육, 비영리, 상업, 정부)</li>
  <li><em>(선택)</em> 홈페이지 링크</li>
  <li>기관이 데비안을 어떻게 사용하는지 하나 또는 두 문장.
워크스테이션/서버 수, 소프트웨어(버전 불필요),
데비안을 선택한 이유.
  </li>
</ol>
